package com.example.a0004306.automationtest;

import android.accessibilityservice.AccessibilityService;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.annotation.UiThread;
import android.util.Log;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityWindowInfo;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.LongFunction;

import static android.view.accessibility.AccessibilityNodeInfo.ACTION_CLICK;
import static android.view.accessibility.AccessibilityNodeInfo.ACTION_SCROLL_BACKWARD;
import static android.view.accessibility.AccessibilityNodeInfo.ACTION_SCROLL_FORWARD;
import static android.view.accessibility.AccessibilityNodeInfo.ACTION_SET_TEXT;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
public class AccessibilitySampleService extends AccessibilityService {

    @Override
    public AccessibilityNodeInfo getRootInActiveWindow() {
        return super.getRootInActiveWindow();
    }

    @Override
    public List<AccessibilityWindowInfo> getWindows() {
        return super.getWindows();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("creating", "onCreate: ");
        new Thread(() -> {
            while (isRunning) {
                try {
                    if (!thisActivityClassName.equals("com.tencent.mm.ui.LauncherUI")) {
                        Thread.sleep(1000 * 1);
                        continue;
                    }
                    if (System.currentTimeMillis() - lastLauncherActionTime > 1000 * 10) {
                        if (thisActivityClassName.equals("com.tencent.mm.ui.LauncherUI")) {
                            List<AccessibilityNodeInfo> list = getRootInActiveWindow().findAccessibilityNodeInfosByText("发现");
                            list.get(0).getParent().getParent().getParent().getChild(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                            Log.e("xx", "weixin is clicked");
                            checkActivityAction("com.tencent.mm.ui.LauncherUI");
                            Thread.sleep(1000 * 5);
                        }
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();


    }

    public static List<String> usrNameList = new ArrayList<>();
    public static int addflag = 0;
    public static int downflag = 0;
    public static int changeTxtFlag = 0;
    public static int chooseInfoFlag = 0;
    String thisActivityClassName = "";

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        Log.i("event", event.toString());
        if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) {
            thisActivityClassName = event.getClassName().toString();
            checkActivityAction(thisActivityClassName);
        }


    }

    long lastLauncherActionTime = 0;

    public void checkActivityAction(String className) {
        try {
            AccessibilityNodeInfo root = getRootInActiveWindow();

            if (className.equals("com.tencent.mm.ui.LauncherUI")) {
                lastLauncherActionTime = System.currentTimeMillis();
                List<AccessibilityNodeInfo> nodeInfoContacts = getRootInActiveWindow().findAccessibilityNodeInfosByText("发现");
                if (downflag == 1) {
                    downflag = 0;
                    Log.e("xx", "com.tencent.mm.ui.LauncherUI back");
                    performGlobalAction(GLOBAL_ACTION_BACK);
                }
                if (addflag == 1) {
                    addflag = 0;
                    nodeInfoContacts.get(0).getParent().getParent().getParent().getChild(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                }
            //    nodeInfoContacts.get(0).getParent().getParent().getParent().getChild(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                if (nodeInfoContacts.get(0).getParent().getParent().getParent().getChild(1).getChild(0).getChild(0).getChildCount() == 2) {
                    //继续添加
                    List<AccessibilityNodeInfo> nodeInfoContact = getRootInActiveWindow().findAccessibilityNodeInfosByText("发现");
                    if (nodeInfoContact.size() == 0) {
                        Log.e("xx", "not in com.tencent.mm.ui.LauncherUI");
                    } else {
                        nodeInfoContact.get(0).getParent().getParent().getParent().getChild(1).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        Log.e("xx", "clicked tongxunlu");
                        //点击新的朋友
                        List<AccessibilityNodeInfo> nodeInfoNewFriend = getRootInActiveWindow().findAccessibilityNodeInfosByText("群聊");
                        if (nodeInfoNewFriend.size() == 0) {
                            Log.e("xx", "not in com.tencent.mm.ui.LauncherUI 2");
                        } else {
                            nodeInfoNewFriend.get(0).getParent().getParent().getParent().getParent().getParent().getChild(1).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                            addflag = 1;
                            Log.e("xx", "clicked xindepengyou");
                        }
                    }
                } else if (nodeInfoContacts.get(0).getParent().getParent().getParent().getChild(1).getChild(0).getChild(0).getChildCount() == 1 && usrNameList.size() != 0) {
                    List<AccessibilityNodeInfo> nodeInfoChatList = getRootInActiveWindow().findAccessibilityNodeInfosByText("发现");
                    nodeInfoChatList.get(0).getParent().getParent().getParent().getChild(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                    if (nodeInfoChatList.size() == 0) {
                        Log.e("xx", "node is 0");
                    } else {
                        Log.e("xx", "node is " + nodeInfoChatList.size());
                    }
                    AccessibilityNodeInfo view = nodeInfoChatList.get(0).getParent().getParent().getParent().getParent().getParent().getChild(0).getChild(0).getChild(0);
                    int index = 0;
                    for (int i = 0; i < 4; i++) {
                        if (view.getChild(i).getClassName().equals("android.widget.ListView")) {
                            index = i;
                            break;
                        }
                    }
                    AccessibilityNodeInfo nodeInfoChatLists = view.getChild(index);

                    nodeInfoChatLists.getChild(11).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                    new Thread(() -> {
                        try {
                            Thread.sleep(300);
                            List<AccessibilityNodeInfo> nodeInfochats = getRootInActiveWindow().findAccessibilityNodeInfosByText("test");
                            if (nodeInfochats.size() == 0) {
                                Log.e("xx", "not have test ");
                            } else {
                                Log.e("xx", " have test " + nodeInfochats.size());

                            }
                            nodeInfochats.get(1).getParent().getParent().getParent().getChild(1).getChild(0).getChild(0).getChild(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    }).start();
                    Log.e("xx", "listview count is click");
                }

                //刚登陆点击通讯录并点击新的朋友
//                if(addflag==0){
////                    List<AccessibilityNodeInfo> nodeInfoContact= getRootInActiveWindow().findAccessibilityNodeInfosByText("发现");
////                    if (nodeInfoContact.size()==0) {
////                        Log.e("xx","not in com.tencent.mm.ui.LauncherUI");
////                    }else {
////                        nodeInfoContact.get(0).getParent().getParent().getParent().getChild(1).performAction(AccessibilityNodeInfo.ACTION_CLICK);
////                        Log.e("xx","clicked tongxunlu");
////                        //点击新的朋友
////                        List<AccessibilityNodeInfo> nodeInfoNewFriend=getRootInActiveWindow().findAccessibilityNodeInfosByText("群聊");
////                        if(nodeInfoNewFriend.size()==0){
////                            Log.e("xx","not in com.tencent.mm.ui.LauncherUI 2");
////                        }else {
////                            nodeInfoNewFriend.get(0).getParent().getParent().getParent().getParent().getParent().getChild(1).performAction(AccessibilityNodeInfo.ACTION_CLICK);
////                            Log.e("xx","clicked xindepengyou");
////
////                        }
////                    }
////                }else if(addflag==1){
//                List<AccessibilityNodeInfo> nodeInfoChatList = getRootInActiveWindow().findAccessibilityNodeInfosByText("发现");
//                if(nodeInfoChatList.size()==0){
//                    Log.e("xx","node is 0");
//                }else{
//                    Log.e("xx","node is "+nodeInfoChatList.size());
//                }
//                AccessibilityNodeInfo view = nodeInfoChatList.get(0).getParent().getParent().getParent().getParent().getParent().getChild(0).getChild(0).getChild(0);
//                int index=0;
//                for(int i=0;i<4;i++){
//                    if(view.getChild(i).getClassName().equals("android.widget.ListView")){
//                        index=i;
//                        break;
//                    }
//                }
//                AccessibilityNodeInfo nodeInfoChatLists=view.getChild(index);
//
//                nodeInfoChatLists.getChild(10).performAction(AccessibilityNodeInfo.ACTION_CLICK);
//                new Thread(() -> {
//                    try {
//                        Thread.sleep(300);
//                        List<AccessibilityNodeInfo> nodeInfochats = getRootInActiveWindow().findAccessibilityNodeInfosByText("test");
//                        if(nodeInfochats.size()==0){
//                            Log.e("xx","not have test ");
//                        }else {
//                            Log.e("xx"," have test "+nodeInfochats.size());
//
//                        }
//                        nodeInfochats.get(1).getParent().getParent().getParent().getChild(1).getChild(0).getChild(0).getChild(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//
//                }).start();
//
//
//                Log.e("xx", "listview count is click");


            } else if (className.equals("com.tencent.mm.plugin.subapp.ui.friend.FMessageConversationUI")) {
                //这是新的朋友界面
                //不用循环接受 直接只加第一个用户直到没有后返回
                //接受时得获取用户的姓名

                new Thread(() -> {
                    try {
                        Thread.sleep(2000);
                        List<AccessibilityNodeInfo> nodeInfoAccept = root.findAccessibilityNodeInfosByText("接受");
                        if (nodeInfoAccept.size() == 0) {
                            if (thisActivityClassName.equals("com.tencent.mm.plugin.subapp.ui.friend.FMessageConversationUI"))
                                performGlobalAction(GLOBAL_ACTION_BACK);
                        }


                        String name = nodeInfoAccept.get(0).getParent().getParent().getChild(0).getChild(0).getText().toString();
                        //  usrNameList.add(name);
                        nodeInfoAccept.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        Log.e("xx", "clicked jieshou");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }).start();

            } else if (className.equals("com.tencent.mm.plugin.profile.ui.SayHiWithSnsPermissionUI")) {
                //点击完成
                List<AccessibilityNodeInfo> nodeinfoFinish = getRootInActiveWindow().findAccessibilityNodeInfosByText("完成");
                if (nodeinfoFinish.size() == 0) {
                    Log.e("xx", "not in com.tencent.mm.plugin.profile.ui.SayHiWithSnsPermissionUI");
                } else {
                    nodeinfoFinish.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                }
            } else if (className.equals("com.tencent.mm.plugin.profile.ui.ContactInfoUI")) {
                //到了用户的个人信息界面，直接返回上一级
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(4000);
                            if (chooseInfoFlag == 0) {
                                //设置备注
                                List<AccessibilityNodeInfo> nodeInfoList = getRootInActiveWindow().findAccessibilityNodeInfosByText("设置备注和标签");
                                nodeInfoList.get(0).getParent().getParent().getParent().getParent().getParent().performAction(AccessibilityNodeInfo.ACTION_CLICK);
                            } else if (chooseInfoFlag == 1) {
                                //修改完返回
                                if (thisActivityClassName.equals("com.tencent.mm.plugin.profile.ui.ContactInfoUI")) {
                                    Log.e("xx", "com.tencent.mm.plugin.profile.ui.ContactInfoUI back");
                                    chooseInfoFlag = 0;
                                    performGlobalAction(GLOBAL_ACTION_BACK);
                                }
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();

            } else if (className.equals("com.tencent.mm.chatroom.ui.ChatroomInfoUI")) {
                //拉群对话框
                if (usrNameList.size() == 0) {
//                    List<AccessibilityNodeInfo> nodeInfosReturn=getRootInActiveWindow().findAccessibilityNodeInfosByText("聊天信息");
//                    // nodeInfosReturn.get(0).getParent().getParent().getChild(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
//                    nodeInfosReturn.get(0).getParent().getParent().getChild(0).setClickable(true);
                    downflag = 1;
                    Log.e("xx", "rerturn ");
                    performGlobalAction(GLOBAL_ACTION_BACK);

                } else if (usrNameList.size() != 0) {
                    Log.e("xx", "rerturn " + usrNameList.size());

//                    List<AccessibilityNodeInfo> nodeInfoChooseFun=getRootInActiveWindow().findAccessibilityNodeInfosByText("群聊名称");
//                    if(nodeInfoChooseFun.size()==0){
//                        Log.e("xx","not have qunliao ");
//                    }else {
//                        Log.e("xx"," have qunliao ");
//
//                    }
//                    String id=nodeInfoChooseFun.get(0).getParent().getParent().getParent().getParent().getParent().getChild(0).getChild(0).getChild(0).getViewIdResourceName();
//                    Log.e("xx"," resourceName: "+id);
//
//                    List<AccessibilityNodeInfo> nodeInfoChooseAdd=getRootInActiveWindow().findAccessibilityNodeInfosByViewId(""+id);
//
//                    int num=nodeInfoChooseAdd.size();
//                    nodeInfoChooseAdd.get(num-2).getParent().performAction(AccessibilityNodeInfo.ACTION_CLICK);
                    //  thisActivityClassName=event.getClassName().toString();

                    new Thread(() -> {
                        while (thisActivityClassName.equals("com.tencent.mm.chatroom.ui.ChatroomInfoUI")) {
                            try {
                                ArrayList<AccessibilityNodeInfo> list = new ArrayList<>();
                                AccessibilityNodeInfo roots = getRootInActiveWindow();
                                Thread.sleep(1000);
                                Log.e("xx", "" + 1);
                                if (roots == null) continue;
                                Log.e("xx", "" + 2);
                                searchNodeInfo(list, roots, ListView.class);
                                Log.e("xx", "" + 3);
                                Thread.sleep(1000);
                                if (list.size() != 0) {
                                    ArrayList<AccessibilityNodeInfo> list3 = new ArrayList<>();
                                    Log.e("xx", "" + 4);
                                    searchNodeInfo(list3, list.get(0), RelativeLayout.class);
                                    Log.e("xx", "" + 5);
                                    Thread.sleep(500);
                                    if (list3.size() > 0) {
                                        //
                                        Log.e("xx", "" + 6);
                                        AccessibilityNodeInfo nodeInfo = null;
                                        Log.e("xx", "size is ：" + list3.size());
                                        for (int m = list3.size() - 1; m > 0; m--) {
                                            if (list3.get(m).getChildCount() != 2) continue;
                                            else {
                                                if (list3.get(m).getChild(1).getChildCount() == 0) {
                                                    Log.e("xx", "" + 7);
                                                    nodeInfo = list3.get(m);
                                                } else {
                                                    break;
                                                }

                                            }
                                        }
                                        Log.e("xx", "get child count:" + nodeInfo.getChildCount());
                                        Log.e("xx", "getChild " + nodeInfo.getChild(1).getChildCount());
                                        if (nodeInfo.getChildCount() != 2 || nodeInfo.getChild(1).getChildCount() != 0) {

                                            Log.e("xx", "error found");
                                            list.get(0).performAction(ACTION_SCROLL_FORWARD);
                                            try {
                                                Thread.sleep(500);
                                            } catch (InterruptedException e) {
                                                e.printStackTrace();
                                            }
                                        } else {
                                            nodeInfo.performAction(AccessibilityNodeInfo.ACTION_CLICK);

                                            Log.e("xx", "success");
                                        }
                                    } else {

                                        list.get(0).performAction(ACTION_SCROLL_BACKWARD);
                                        try {
                                            Thread.sleep(500);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        list.get(0).performAction(ACTION_SCROLL_BACKWARD);

                                        try {
                                            Thread.sleep(500);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        list.get(0).performAction(ACTION_SCROLL_BACKWARD);

                                        try {
                                            Thread.sleep(500);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        Log.e("xx", "RelativeLayout size==0");
                                    }


                                } else {

                                    Log.e("xx", " ListView size= " + list.size());

                                    break;
//                        list.get(0).performAction(ACTION_SCROLL_BACKWARD);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }).start();
                }

            } else if (className.equals("com.tencent.mm.ui.contact.SelectContactUI")) {
                List<AccessibilityNodeInfo> nodeInfoSearch = getRootInActiveWindow().findAccessibilityNodeInfosByText("#");
                //   usrNameList.add("");
                new Thread(() -> {
                    for (String name : usrNameList) {
                        try {
                            Log.e("xx", "can setname");
                            if (nodeInfoSearch.size() == 0) {
                                Log.e("xx", "no search");
                            } else {
                                Log.e("xx", "can search" + nodeInfoSearch.size());
                                for (AccessibilityNodeInfo item : nodeInfoSearch) {
                                    Log.e("xx", "baoming :" + item.getViewIdResourceName());
                                }
                                Thread.sleep(1000);
                                setAccessTxt(nodeInfoSearch.get(0).getParent().getParent().getParent().getChild(2).getChild(0).getChild(0).getChild(0).getChild(1), name);
                                Log.e("xx", "名字：" + name);
                                Thread.sleep(2000);
                                List<AccessibilityNodeInfo> nodeInfoName = getRootInActiveWindow().findAccessibilityNodeInfosByText(name);
                                if (nodeInfoName.size() != 0) {
                                    Log.e("xx", "搜索后的名字有:" + nodeInfoName.size());
                                    AccessibilityNodeInfo nodeInfo = nodeInfoName.get(0).getParent().getParent().getParent().getParent().getParent();
                                    Log.i("xx", nodeInfo.getClassName().toString());
                                    nodeInfo.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                    Log.e("xx", "is selected");
                                }
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    List<AccessibilityNodeInfo> nodeInfoSure = getRootInActiveWindow().findAccessibilityNodeInfosByText("确定");
                    Log.e("xx", "" + 8);
                    usrNameList.clear();
                    try {
                        Thread.sleep(1000);
                        Log.e("xx", "" + 9);
                        nodeInfoSure.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Log.e("xx", "press sure");
//                    try {
//                        Thread.sleep(1000);
//                        performGlobalAction(GLOBAL_ACTION_BACK);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }

                }).start();
            } else if (className.equals("com.tencent.mm.ui.contact.ContactRemarkInfoModUI")) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (changeTxtFlag == 0) {
                            List<AccessibilityNodeInfo> list = getRootInActiveWindow().findAccessibilityNodeInfosByText("备注名");
                            list.get(0).getParent().getChild(1).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                            List<AccessibilityNodeInfo> listFlush = getRootInActiveWindow().findAccessibilityNodeInfosByText("添加标签对联系人进行分类");
                            listFlush.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        } else if (changeTxtFlag == 1 && thisActivityClassName.equals("com.tencent.mm.ui.contact.ContactRemarkInfoModUI")) {
                            long currentTimeMillis = System.currentTimeMillis();
                            String name = timeToString(currentTimeMillis);
                            usrNameList.add(name);
                            Log.e("xx", "add name is :" + name);
                            List<AccessibilityNodeInfo> list = getRootInActiveWindow().findAccessibilityNodeInfosByText("备注名");
                            Log.e("xx", "List size is:" + list.size());
                            Log.e("xx", "class name:" + list.get(0).getParent().getChild(1).getClassName());
                            setAccessTxt(list.get(0).getParent().getChild(1), name);
                            changeTxtFlag = 0;
                            List<AccessibilityNodeInfo> listFinish = getRootInActiveWindow().findAccessibilityNodeInfosByText("完成");
                            chooseInfoFlag = 1;
                            listFinish.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);

                        }
                    }
                }).start();

            } else if (className.equals("com.tencent.mm.plugin.label.ui.ContactLabelUI")) {
                new Thread(() -> {
                    changeTxtFlag = 1;
                    try {
                        Thread.sleep(300);
                        performGlobalAction(GLOBAL_ACTION_BACK);
                        Thread.sleep(300);
                        performGlobalAction(GLOBAL_ACTION_BACK);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }).start();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    void setAccessTxt(AccessibilityNodeInfo nodeInfo, String name) {
        if (nodeInfo != null & nodeInfo.getClassName().equals("android.widget.EditText")) {
            Bundle arguments = new Bundle();
            arguments.putCharSequence(AccessibilityNodeInfo
                    .ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, "" + name);
            nodeInfo.performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
        }
    }

    public static void searchNodeInfo(List<AccessibilityNodeInfo> list, AccessibilityNodeInfo nodeInfo, Class<? extends View> clazz) {
        for (int i = 0; i < nodeInfo.getChildCount(); i++) {
//            Log.e("xx",nodeInfo.getChild(i).getClassName()+"   map:"+clazz.getName());
            AccessibilityNodeInfo nodeInfo1 = nodeInfo.getChild(i);
            if (nodeInfo1 == null) break;
            if (nodeInfo1.getClassName().equals(clazz.getName())) {
                list.add(nodeInfo.getChild(i));
            } else {
                searchNodeInfo(list, nodeInfo.getChild(i), clazz);
            }
        }
    }

    public static String timeToString(long currentTimeMillis) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(currentTimeMillis);
        String format = sdf.format(date);
        return format;
    }

    boolean isRunning = true;

    @Override
    public void onInterrupt() {
        isRunning = false;

    }
}
